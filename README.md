# Dependencies
This polybar script require the following libs:
- `inotify-tools`
- `xinput`
- `xdotool`
- `xclip`
- `xwd`
- `imagemagick`

# Module
```ini
[module/colorpicker]
type = custom/script
exec = "~/.config/polybar/scripts/colorpicker.sh"
click-left = ~/.config/polybar/scripts/colorpicker.sh --pick
click-right = "kill -USR1 $(pgrep --oldest --parent %pid%) && ~/.config/polybar/scripts/colorpicker.sh --update"
double-click-right = "kill -USR2 $(pgrep --oldest --parent %pid%) && ~/.config/polybar/scripts/colorpicker.sh --update"
scroll-up = "echo "previous_color" > /tmp/i3colorpicker && kill -USR1 $(pgrep --oldest --parent %pid%) && ~/.config/polybar/scripts/colorpicker.sh --update"
scroll-down = "echo "next_color" > /tmp/i3colorpicker && kill -USR1 $(pgrep --oldest --parent %pid%) && ~/.config/polybar/scripts/colorpicker.sh --update"
tail = true
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
label = %output%
```
# Features
## Pick a color on your screen
To pick a color, left click on the polybar colorpicker module.  
After that, click on the color you want to pick.  

## Change display mode
To change the display mode of a color, right click on the polybar colorpicker module.  
The available modes are :  
- Hexadecimal
- RGB

## Copy current color into clipboard
To copy the color (with the current displaying mode), double right click on the polybar colorpicker module.  

## Open color palette
To open color palette, double left click on the polybar colorpicker module.  
If you want a popup for color palette, add this on your i3 config :  
```
for_window [class="Yad"] floating enable
```
## Navigate through the color list
You can navigate through the color list using the scroll wheel  

# Todo
Nothing for the moment, but you can suggest any change / new features
